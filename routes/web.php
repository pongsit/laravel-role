<?php

use Illuminate\Support\Facades\Route;
use Pongsit\Role\Http\Controllers\RoleController;

Route::prefix('role')->middleware(['userCan:manage_role'])->group(function () {
	Route::get('/user/{role:name}', [RoleController::class, 'user'])->name('role.user');
});


// Route::get('/role', [RoleController::class, 'index'])->name('role')->middleware('auth');
// Route::get('/role/show/avatar/{name}/{size}', [RoleController::class, 'showAvatar'])->name('role.showAvatar');
// Route::post('/role/update/avatar/{id}', [RoleController::class, 'updateAvatar']);

// Route::get('/admin/dashboard', [RoleController::class, 'adminDashboard'])->name('admin.dashboard')->middleware('isAdmin');