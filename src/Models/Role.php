<?php

namespace Pongsit\Role\Models;

use Pongsit\Role\Database\Factories\RoleFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;


class Role extends Model
{
  use HasFactory;

  // Disable Laravel's mass assignment protection
  protected $guarded = [];

  public $all;

  protected static function newFactory()
  {
    return RoleFactory::new();
  }

  public function users(){
    return $this->belongsToMany('Pongsit\System\Models\User')->withTimestamps();
  }

  public function abilities(){
    return $this->belongsToMany('Pongsit\Ability\Models\Ability')->withTimestamps();
  }

  public function getId($role_name){
      $role = $this->where('name',$role_name)->first();
      if(!empty($role->id)){
        return $role->id;
      }
      return false;
  }

  // public function getAll(){
  //     if(!isset($this->all)){
  //       $this->all = $this->all();
  //     }
  //     return $this->all;
  // }
}
