<?php

namespace Pongsit\Role\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Response;
use Pongsit\User\Models\User;
use Throwable;
use Image;
use Pongsit\Role\Models\Role;
use Pongsit\Ability\Models\Ability;
use Pongsit\System\Models\System;

class RoleController extends Controller
{

	public function index(){
		if(!user()->can('manage_role')){
    		return back()->with(['error'=>'คุณไม่มีสิทธิ์ดำเนินการครับ']);
    	}
		$variables = [];
        $append_query_strings = [];

        // การจัดเรียงข้อมูล
        $order_by = 'power';
        if(!empty($_GET['order_by'])){
            $order_by = $_GET['order_by'];
            $append_query_strings['order_by'] = 'order_by='.$_GET['order_by'];
        }
        $variables['order_by'] = $order_by;

        $order_by_show = '';
        switch ($order_by) {
            case 'name':
                $order_by_show = 'ชื่อแสดง';
                $sort_shows['desc'] = 'ย้อนตัวอักษร';
                $sort_shows['asc'] = 'ตามตัวอักษร';
                break;
            case 'created_at':
                $order_by_show = 'เวลาเข้าร่วม';
                $sort_shows['desc'] = 'จากใหม่ไปเก่า';
                $sort_shows['asc'] = 'จากเก่าไปใหม่';
                break;
            case 'updated_at':
                $order_by_show = 'Update ล่าสุด';
                $sort_shows['desc'] = 'จากใหม่ไปเก่า';
                $sort_shows['asc'] = 'จากเก่าไปใหม่';
                break;
            case 'status':
                $order_by_show = 'สถานะ';
                $sort_shows['desc'] = 'ปกติขี้นก่อน';
                $sort_shows['asc'] = 'ยกเลิกขี้นก่อน';
                break;
            default:
                $order_by_show = 'รหัส';
                $sort_shows['desc'] = 'จากมากไปน้อย';
                $sort_shows['asc'] = 'จากน้อยไปมาก';
                break;
        }

        $sort = 'desc';
        $append_query_strings['sort'] = 'sort='.$sort;
        $sort_show = $sort_shows[$sort];
        if(!empty($_GET['sort'])){
            $sort = $_GET['sort'];
            $append_query_strings['sort'] = 'sort='.$_GET['sort'];
            $sort_show = $sort_shows[$sort];
        }
        $variables['sort_reverse'] = '';
        if($append_query_strings['sort']!='sort=asc'){
            $variables['sort_reverse']='sort=asc';
        }else{
            $variables['sort_reverse']='sort=desc';
        }
        $variables['sort'] = $sort;

        $variables['sort'] = $sort;
        $variables['sort_show'] = $sort_show;

        $order_by_show .= $sort_show;
        $variables['order_by_show'] = $order_by_show;

        // ข้อมูลเฉพาะคำที่ Search
        $search = '';
        if(!empty($_GET['search'])){
            $search = $_GET['search'];
            $append_query_strings['search'] = 'search='.$_GET['search'];
        }
        $variables['search'] = $search;

        // ข้อมูลเฉพาะ User
        if(!empty($_GET['user_id'])){
            $user_id = $_GET['user_id'];
        }

        $variables['append_query_strings'] = $append_query_strings;

        $item_per_page = 30;
        if(empty($search)){
            $variables['members'] = Role::orderBy($order_by,$sort)->paginate($item_per_page);
        }else{
            $variables['members'] = Role::where('name','like','%'.$search.'%')
                ->orWhere('name','like','%'.$search.'%')
                ->orWhere('created_at','like','%'.$search.'%')
                ->orderBy($order_by,$sort)
                ->paginate($item_per_page);
        }

        $variables['paginations'] = $variables['members']->toArray();

        $variables['count_result'] = $variables['paginations']['total'];

        // dd($variables['paginations']);
        $variables['paginations']['append_query_string'] = '';
        if(!empty($append_query_strings)){
            $variables['paginations']['append_query_string'] = '&'.implode('&', $append_query_strings);
        }
        $variables['pagination'] = view('system::components.pagination',$variables['paginations']);
        
        $variables['order_number'] = 1;
        if(!empty($_GET['page'])){
            $variables['order_number'] = ($_GET['page']-1)*$item_per_page+1;
        }

        $variables['currentUrl'] = url()->current();

		return view('role::index',$variables);
	}

	public function roleAbility(){
		if(!(user()->can('manage_role') && user()->can('manage_ability'))
		){
    		return abort(403);
    	}

		$variables['user'] = user();
		$variables['roles'] = role()->where('status',1)->orderBy('power')->get();

		$ability = new Ability();
		$abilities = $ability->get();
		$variables['ability_checks'] = array();
		foreach($abilities as $v){
			foreach($variables['roles'] as $__v){
				$variables['ability_checks'][$v->id][$__v->id] = 0;
			}
			foreach($v->roles()->get() as $_v){
				$variables['ability_checks'][$v->id][$_v->id] = 1;
			}
		}
		$variables['abilities'] = $abilities;
		return view('role::ability.index',$variables);
	}

	public function updateAbility(Request $request){
		if(!user()->can('manage_role')){
    		return back()->with(['error'=>'คุณไม่มีสิทธิ์ดำเนินการครับ']);
    	}
		$inputs = $request->all();
		unset($inputs['_token']);
		$preps = array();
		foreach($inputs as $k=>$v){
			$explodes = explode('_',$k);
			$preps[$explodes[0]][] = $explodes[1];
		}
		$ability = new Ability();
		foreach($preps as $k => $vs){
			$ability->find($k)->roles()->sync($vs);
		}

		return back()->with(['success'=>'ปรับปรุงข้อมูลเรียบร้อย']);
	}

	public function user(Role $role){
		if(!user()->can('manage_role')){
    		return back()->with(['error'=>'คุณไม่มีสิทธิ์ดำเนินการครับ']);
    	}

    	$paginator = DB::table('role_user')->where('role_id',$role->id)->orderBy('updated_at')->simplePaginate(15);
    	$variables['users'] = $paginator;
		$variables['previousPageUrl'] = $paginator->previousPageUrl();
		$variables['nextPageUrl'] = $paginator->nextPageUrl();
		$variables['title'] = $role->name_show;

		return view('role::user.index',$variables);
	}

	public function store(Request $request)
    {
    	if(!user()->can('manage_role')){
    		return back()->with(['error'=>'คุณไม่มีสิทธิ์ดำเนินการครับ']);
    	}

    	$request->validate([
			'name' => 'required',
			'name_show' => 'required',
		],[
			'name.required'=>'กรุณากรอกชื่อสำหรับ DB',
			'name_show.required'=>'กรุณากรอกชื่อแสดง',
			'power.min'=>'กรุณากรอก power เป็นตัวเลขระหว่าง 1-100',
			'power.max'=>'กรุณากรอก power เป็นตัวเลขระหว่าง 1-100',
		]);

		$infos = [];

        if(!empty($request->power)){
            $infos['power'] = $request->power;
        }
        
        $infos['name_show'] = $request->name_show;

		if(isset($request->status)){
			$infos['status'] = $request->status;
		}

		// ถ้ามีชื่อเดียวกันอยู่ใน database จะ update ด้วย infos 
		// ถ้าไม่มีจะ create new entry
		$role = Role::updateOrCreate(['name' => $request->name], $infos);

		$role = new Role;
		cache(['role' => $role]);

    	return back()->with(['success'=>'ปรับปรุงข้อมูลเรียบร้อย']);
    }

 //    public function check($role_name){
	// 	$user = Auth::user();
	// 	if(empty($user)){
	// 		return false;
	// 	}
	// 	$user_id = $user->id;
	// 	$numberOfRow = Role::where('user_id', $user_id)->where('role',$role_name)->count();
		
	// 	if($numberOfRow > 0){
	// 		return true;
	// 	}else{
	// 		return false;
	// 	}
	// }

	// public function adminMember(){
	// 	// $system = new System();
	// 	// $users = $system->getUser();
	// 	// $members = $system->getUsers()->sortByDesc('id');
	// 	// $infos = $users;

	// 	// $infos['members'] = $members;
	// 	$user = new User();
	// 	$infos['members'] = User::all();
	// 	return view(config('role.theme').'::member',$infos);
	// }

	// public function adminDashboard(){
	// 	$system = new System();
	// 	$user = session('user');
	// 	$infos = $user;
	// 	$infos['numberOfUser'] = number_format($system->getNumberOfUser());
	// 	return view(config('role.theme').'::dashboard',$infos);
	// }
}
