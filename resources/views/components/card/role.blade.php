@if(user()->can('manage_role'))
    <div class="card shadow mb-3">
        <div class="card-header pt-3 pb-3 d-flex justify-content-between">
            <div><h6 class="m-0 font-weight-bold">บทบาท</h6></div>
            <div><a href="{{route('role.index')}}" ><i class="fas fa-edit"></i> </a></div>
        </div>
        <div class="card-body">
            <div class="">
                @foreach(role()->where('status',1)->orderBy('power','desc')->get()->load('users') as $v)
                    <a href="{{route('role.user',$v)}}">{{$v->name_show}} ({{count($v->users)}})</a> |
                @endforeach
            </div>
            @if(user()->can('manage_ability'))
                <hr />
                <div>
                    <a href="{{route('role.ability')}}" >จัดการความสามารถ</a>
                </div>
            @endif
        </div>
    </div>
@endif