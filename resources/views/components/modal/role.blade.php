<div class="modal fade" id="{{$id}}" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{{$title}}</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form method="post" action="{{route('role.store')}}">
          @csrf
          <div class="d-flex mb-2">
            <div style="width:80px;">ชื่ออังกฤษ*:</div>
            <div class="flex-fill">
              <input type="text" name="name" class="form-control" placeholder="เช่น student" value="{{$name ?? ''}}">
            </div>
          </div>
          <div class="d-flex mb-2">
            <div style="width:80px;">ชื่อแสดง*:</div>
            <div class="flex-fill">
              <input type="text" name="name_show" class="form-control" placeholder="เช่น นักเรียน" value="{{$nameShow ?? ''}}">
            </div>
          </div>
          <div class="d-flex mb-2">
            <div style="width:80px;">Power:</div>
            <div class="flex-fill">
              <input type="text" name="power" class="form-control" placeholder="ตัวเลขระหว่าง 1-100 เช่น 40" value="{{$power ?? ''}}">
            </div>
          </div>
          <div class="d-flex mb-2">
            <div style="width:80px;">สถานะ:</div>
            <div class="flex-fill">
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="status" id="inlineRadio1" value="1" 
                  @if($status) checked @endif
                >
                <label class="form-check-label" for="inlineRadio1">ปกติ</label>
              </div>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="status" id="inlineRadio2" value="0" 
                  @empty($status) checked @endempty
                >
                <label class="form-check-label" for="inlineRadio2">ยกเลิก</label>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer d-flex">
        <button type="button" class="btn btn-secondary form-control" style="width:120px;" data-bs-dismiss="modal">ยกเลิก</button>
        <button type="button" class="btn btn-primary {{$id}}-submit form-control" style="width:120px;" >ส่งข้อมูล</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    $(function () {
        $('body').on('click', '.{{$id}}-submit', function (e) {
            $('#{{$id}}').find('form').submit();
            $('#{{$id}}').modal('hide');
        });
    });
</script>