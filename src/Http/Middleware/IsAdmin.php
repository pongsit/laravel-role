<?php

namespace Pongsit\Role\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        //return $next($request);
        if(user()->isAdmin()){
            return $next($request);
        }
        return redirect('/')->with(['error'=>'คุณไม่มีสิทธิ์เข้าใช้หน้านี้ครับ']);
    }
}
