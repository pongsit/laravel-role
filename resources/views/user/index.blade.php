@extends('layouts.main')

@section('content')

<div class="container">
    <x:section.type1 title="{{$title}}" />
    <div class="row justify-content-center">
        @foreach($users as $v)
        @php 
            $u = \Pongsit\User\Models\User::find($v->user_id); 
        @endphp
        <div class="col-6 col-md-3 col-xl-2 pb-3">
            <a href="{{route('user.show',$u)}}" >
                <div class="card shadow mb-3">
                    <div class="card-header pt-3 pb-3 d-flex justify-content-between">
                        <div><h6 class="m-0 font-weight-bold" style="width:120px; overflow:hidden; white-space:nowrap; text-overflow:ellipsis;">{{$u->name}}</h6></div>
                    </div>
                    <div class="card-body">
                        <x-profile::picture :user="$u" /> 
                        บทบาท: 
                        @foreach($u->roles()->get() as $r)
                            {{$r->name}} 
                        @endforeach
                    </div>
                </div>
            </a>
        </div>
        @endforeach 
    </div>
</div>

<div class="container">
    <div class="d-flex justify-content-center">
        @if(!empty($previousPageUrl))
            <div class="me-3" style="width:120px;">
                <a class="btn btn-primary form-control" href="{{$previousPageUrl}}">ก่อนหน้า</a>
            </div>
        @endif
        @if(!empty($nextPageUrl))
            <div style="width:120px;">
                <a class="btn btn-primary form-control" href="{{$nextPageUrl}}">ถัดไป</a>
            </div>
        @endif
    </div>
</div>

@stop