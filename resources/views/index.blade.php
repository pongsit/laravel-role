@extends('layouts.main')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col col-lg-6">
            <div class="card shadow mb-2">
                <div class="card-header pt-3 pb-3 justify-content-between d-flex">
                    <div class="flex-fill">บทบาท</div>
                    <div>
                        <a href="javascript:;" data-bs-toggle="modal" data-bs-target="#insertRole">
                            <i class="fas fa-plus-circle"></i> เพิ่ม
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row mb-0" style="">   
                        <div class="col">
                            <div class="d-flex justify-content-center">
                                <div class="me-2" style="width: 80px;">
                                    ชื่อ
                                </div>
                                <div class="me-2 flex-fill" style="">
                                    ชื่อแสดง
                                </div>
                                <div class="me-2 text-center" style="width: 60px;">
                                    Power
                                </div>
                                <div class="me-2 text-center" style="width: 40px;">
                                    Users
                                </div>
                                <div class="ms-2 me-2 text-center" style="width:60px;">
                                    สถานะ
                                </div>
                                <div class="">
                                    อื่นๆ
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr />
                    @php 
                        $bg = '';
                        $bg_mem = $bg; 
                    @endphp
                    @foreach($members as $v)
                        <div class="row mb-0 {{$bg}}">   
                            <div class="col">
                                <div class="d-flex justify-content-center">
                                    <div class="me-2" style="width: 80px;">
                                        {{$v->name}}
                                    </div>
                                    <div class="me-2 flex-fill" style="">
                                        {{$v->name_show}}
                                    </div>
                                    <div class="me-2 text-center" style="width: 60px;">
                                        {{$v->power}}
                                    </div>
                                    <div class="me-2 text-center" style="width: 40px;">
                                        {{$v->users()->count()}}
                                    </div>
                                    <div class="ms-2 me-2 text-center" style="width:60px;">
                                        @if($v->status)
                                            ปกติ
                                        @else
                                            <span class="text-danger">ยกเลิก</span>
                                        @endif
                                    </div>
                                    <div class="">
                                        <a href="javascript:;" data-bs-toggle="modal" data-bs-target="#{{$v->name}}">แก้ไข</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <x-role::modal.role
                            id="{{$v->name}}"
                            title="{{$v->name_show}}"
                            status="{{$v->status}}"
                            name="{{$v->name}}"
                            nameShow="{{$v->name_show}}"
                            power="{{$v->power}}"
                        />
                        @php 
                            if(!empty($bg)){
                                $bg = $bg_mem;
                            }else{
                                $bg = 'dark';
                            }
                        @endphp  
                    @endforeach  
                </div>
            </div>
            @php
                echo $pagination;
            @endphp
        </div>
    </div>
</div>

<x-role::modal.role 
    id="insertRole"
    title="เพิ่มบทบาท"
    status="1"
/>
@stop